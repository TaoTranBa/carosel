import React from 'react';
import {SafeAreaView, View, StatusBar} from 'react-native';

import CardComponent from './src/components/CardComponent';

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View>
          <CardComponent />
        </View>
      </SafeAreaView>
    </>
  );
};

export default App;
