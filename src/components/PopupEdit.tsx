import React, {useState} from 'react';
import {StyleSheet, Text} from 'react-native';
import {Input, Button} from 'react-native-elements';
import Dialog, {DialogContent} from 'react-native-popup-dialog';

interface IPropPopup {
  value: string;
  visible: boolean;
  closePopup: any;
  updateInfo: any;
}

const PopupEdit: React.FC<IPropPopup> = ({
  value,
  visible,
  closePopup,
  updateInfo,
}) => {
  const [vals, setVal] = useState<string>('');
  const hanldeOk = () => {
    updateInfo(vals);
  };
  return (
    <>
      <Dialog
        visible={visible}
        dialogTitle={<Text style={styles.titleEdit}>Chỉnh sửa</Text>}
        onTouchOutside={() => {
          closePopup();
          setVal(value);
        }}
        width={0.8}
        height={0.2}>
        <DialogContent>
          <Input
            placeholder="Nhập thông tin"
            style={styles.mt20}
            defaultValue={value}
            onChangeText={(val) => setVal(val)}
          />
          <Button title="Chấp nhận" onPress={hanldeOk} />
        </DialogContent>
      </Dialog>
    </>
  );
};

const styles = StyleSheet.create({
  image: {
    borderRadius: 50,
    width: 100,
    height: 100,
  },
  row: {
    flexDirection: 'row',
  },
  titleEdit: {
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 10,
    fontSize: 20,
  },
  mt20: {
    marginTop: 20,
  },
});

export default PopupEdit;
