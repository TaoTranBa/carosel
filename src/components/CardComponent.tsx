import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Text, FlatList} from 'react-native';
import {Card} from 'react-native-elements';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {getUsers, updateUser} from '../service';
import PopupEdit from './PopupEdit';

const imageDefault =
  'https://nguoi-noi-tieng.com/images/post/tieu-su-nguyen-hoang-kieu-trinh-533126.jpg';

const CardComponent = () => {
  const [visible, setVisiable] = useState<boolean>(false);
  const [value, setValue] = useState<string>('');
  const [keyType, setkeyType] = useState<string>('');
  const [idSelected, setIdSelected] = useState<string>('');

  const [users, setUsers] = useState([]);
  const [page] = useState<number>(1);

  const getData = async () => {
    const res = await getUsers({page});
    setUsers(res.data);
  };

  useEffect(() => {
    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const editInfo = async (item: any, keyT: string) => {
    await setValue(
      keyT === 'name'
        ? item.name
        : keyT === 'birth'
        ? item.birth
        : keyT === 'address'
        ? item.address
        : item.phone,
    );
    await setkeyType(keyT);
    setIdSelected(item.id);
    setVisiable(true);
  };

  const closePopup = () => {
    setVisiable(false);
  };

  const updateInfo = async (val: string) => {
    if (val !== '') {
      const update =
        keyType === 'name'
          ? {name: val}
          : keyType === 'birth'
          ? {birth: val}
          : keyType === 'address'
          ? {address: val}
          : {phone: val};
      await updateUser(idSelected, update);
      await getData();
    }
    setVisiable(false);
  };

  const renderImage = (item: any) => (
    <View>
      <Card>
        <Card.Image
          source={{
            uri: item.avatar ? item.avatar : imageDefault,
          }}
          style={styles.image}
        />
        <Card.Divider />
        <Text style={styles.textCenter}>{item.name}</Text>
        <View style={styles.row}>
          <FontAwesome
            name={'user-circle-o'}
            size={25}
            onPress={() => editInfo(item, 'name')}
          />
          <FontAwesome
            name={'calendar-minus-o'}
            size={25}
            onPress={() => editInfo(item, 'birth')}
          />
          <FontAwesome
            name={'map-marker'}
            size={25}
            onPress={() => editInfo(item, 'address')}
          />
          <FontAwesome
            name={'phone'}
            size={25}
            onPress={() => editInfo(item, 'phone')}
          />
          <FontAwesome name={'lock'} size={25} />
        </View>
      </Card>
    </View>
  );
  return (
    <>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        keyExtractor={(_item, index) => index.toString()}
        data={users}
        renderItem={({item}) => renderImage(item)}
      />

      <PopupEdit
        value={value}
        visible={visible}
        closePopup={closePopup}
        updateInfo={updateInfo}
      />
    </>
  );
};

const styles = StyleSheet.create({
  image: {
    borderRadius: 50,
    width: 100,
    height: 100,
  },
  row: {
    flexDirection: 'row',
    marginTop: 2,
  },
  titleEdit: {
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 10,
    fontSize: 20,
  },
  mt20: {
    marginTop: 20,
  },
  container: {},
  textCenter: {
    textAlign: 'center',
  },
});

export default CardComponent;
