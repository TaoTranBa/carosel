import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://34.87.19.255:3333',
  timeout: 12000,
});

instance.interceptors.response.use(
  (response: any) => {
    return response;
  },
  (error: any) => {
    return Promise.reject(error.response.data);
  },
);

export {instance as CaroselAPI};
