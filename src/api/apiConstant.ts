export const CAROSEL_API = {
  GET_CAROSEL_USER: '/api/user',
  PATCH_CAROSEL_USER: (id: string) => `/api/user/${id}`,
};
