import {CaroselAPI, CAROSEL_API} from '../api';

export interface UserParam {
  page: number;
}

export const getUsers = async (params: UserParam) => {
  const response = await CaroselAPI.get(CAROSEL_API.GET_CAROSEL_USER, {params});

  return response.data;
};
export const updateUser = async (id: string, data: any) => {
  const response = await CaroselAPI.patch(CAROSEL_API.PATCH_CAROSEL_USER(id), {
    ...data,
  });

  return response.data;
};
